package pageObjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utlities.PageFactory;


public class HomePage {

	WebDriver driver;
	
	final String url = "http://link.springer.com/";
	
	public HomePage()
	{
		driver = PageFactory.getWebDriverInstance();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public WebElement get_TextBox_Search()
	{
		return driver.findElement(By.id("query"));
	}

	public void set_TextBox_Search(String content)
	{
		this.get_TextBox_Search().sendKeys(content);
	}
	
	public WebElement get_Button_Search()
	{
		return driver.findElement(By.id("search"));
	}
	
	public void click_Button_Search()
	{
		this.get_Button_Search().click();
	}
	
	public void openPage()
	{
		driver.navigate().to(url);
	}
}

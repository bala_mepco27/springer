package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utlities.PageFactory;

public class ResultPage {

	WebDriver driver;
	
	public ResultPage()
	{
		driver = PageFactory.getWebDriverInstance();
	}
	
	public WebElement get_Label_Result()
	{
		return driver.findElement(By.xpath("//h1[@class = 'number-of-search-results-and-search-terms']/strong"));
	}
	
	public int get_Result_Count()
	{
		return Integer.parseInt(this.get_Label_Result().getText().replaceAll(",", ""));
	}
	
}

package stepDefinition;

import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageObjects.HomePage;
import pageObjects.ResultPage;

public class TestSteps {
	
	HomePage hp;
	ResultPage rp;
	
	public TestSteps()
	{
		hp = new HomePage();
		rp = new ResultPage();
	}
	
	@Given("^User is on Home Page$")
	public void user_is_on_Home_Page() throws Throwable {
		hp.openPage();
	}

	@When("^User search for$")
	public void user_search_for() throws Throwable {
		hp.set_TextBox_Search("*");
		hp.click_Button_Search();
	}

	@Then("^Total number of results should be greater than (\\d+)$")
	public void total_no_of_results_should_be_equal_to(int arg) throws Throwable {
		if(arg>rp.get_Result_Count())
			Assert.fail("Total no. of results is less than expected");
	}
	
}

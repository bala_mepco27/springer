package utlities;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageFactory {

	static WebDriver driver;
	
	private PageFactory(){}
	
	public static WebDriver getWebDriverInstance()
	{
		if(driver == null)
		{
			driver = new FirefoxDriver();
		}
		return driver;
	}
}
